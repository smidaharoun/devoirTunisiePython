import re
import urllib2

from bs4 import BeautifulSoup
from flask import Flask, jsonify
from flask import request



app = Flask(__name__)

main = "https://www.devoir.tn/"
page = urllib2.urlopen(main)
soup = BeautifulSoup(page, 'html.parser')
soup.prettify()


@app.route('/getMenus', methods=['GET'])
def get_menu():
    right_table = soup.find_all('li', class_='sub-menu')
    menus = []
    accueil = soup.find("ul", class_="main-menu").find("li").find("a").contents[1].strip()
    menus.append({"id": 0, "name": accueil, "items ": []})
    id_item = 1
    for row in right_table:
        items = row.find_all("a")
        title = items[0].contents[1].strip()
        del items[0]
        subtitles = []
        for item in items:
            subtitles.append(item.text)

        hashmap = {"id": id_item, "name": title, "items ": subtitles}
        id_item += 1
        menus.append(hashmap)

    resp = jsonify(menus)
    resp.status_code = 200
    return resp


@app.route('/getAccueil', methods=['GET'])
def get_accueil():
    table = soup.find_all("div", {'class': re.compile(r'card pt-inner br-1.*')})
    items = []
    id_item = 0
    for row in table:
        header = row.find("div", {'class': re.compile(r'pti-header bgm.*')})
        number_document = header.find("p").text
        name_document = header.find("div").text
        type_document = header.find("small").text
        footer = row.find("div", {'class': re.compile(r'pti-footer.*')})
        link = footer.find("a").get("href")
        hashmap = {"id": id_item, "name": name_document, "value": number_document, "type": type_document,
                   "link": link}
        id_item += 1
        items.append(hashmap)

    resp = jsonify(items)
    resp.status_code = 200
    return resp


@app.route('/getLevel', methods=['POST'])
def get_level():
    request_link = request.form["link"]
    url_level = main + request_link
    page_level = urllib2.urlopen(url_level)
    soup_level = BeautifulSoup(page_level, 'html.parser')
    soup_level.prettify()
    table = soup_level.find_all("div", {'class': re.compile(r'card br-1 bgb-amber.*')})
    items = []
    id_item = 0
    for row in table:
        header = row.find("div", class_="card-header bgm-amber text-left ch-dark")
        name_document = header.find("h2").contents[0]
        link = header.find("a")
        sub_items = row.find_all("a", class_="list-group-item media")
        list_items = []
        inner_id = 0
        for item in sub_items:
            item_link = item.get("href")
            item_name = item.find("div", class_="lgi-heading").text
            list_items.append({"id": inner_id, "name": item_name, "link": item_link.replace("./", "")})
            inner_id += 1

        hashmap = {"id": id_item, "name": name_document, "link": link.get("href").replace("./", ""), "subjects": list_items}
        id_item += 1
        items.append(hashmap)

    resp = jsonify(items)
    resp.status_code = 200
    return resp


@app.route('/getYear', methods=['POST'])
def get_year():
    request_link = request.form['link']
    url_level = main + request_link
    page_level = urllib2.urlopen(url_level)
    soup_level = BeautifulSoup(page_level, 'html.parser')
    soup_level.prettify()
    table = soup_level.find_all("div", {'class': re.compile(r'card br-1 bgb-amber.*')})
    items = []
    id_item = 0
    for row in table:
        header = row.find("div", class_="card-header bgm-amber text-left ch-dark")
        name_document = header.find("h2").contents[0]
        link = header.find("a")
        sub_items = row.find_all("a", class_="list-group-item media")
        list_items = []
        inner_id = 0
        for item in sub_items:
            item_link = item.get("href")
            item_name = item.find("div", class_="lgi-heading").text
            list_items.append({"id": inner_id, "name": item_name, "link": item_link.replace("../", "")})
            inner_id += 1

        hashmap = {"id": id_item, "name": name_document, "link": link.get("href").replace("../", ""), "items": list_items}
        id_item += 1
        items.append(hashmap)

    resp = jsonify(items)
    resp.status_code = 200
    return resp


@app.route('/getSubject', methods=['POST'])
def get_subject():
    request_link = request.form['link']
    url_level = main + request_link
    page_level = urllib2.urlopen(url_level)
    soup_level = BeautifulSoup(page_level, 'html.parser')
    soup_level.prettify()
    table = soup_level.find_all("div", {'class': re.compile(r'card br-1 bgb-amber.*')})
    items = []
    id_item = 0
    for row in table:
        header = row.find("div", class_="card-header bgm-amber text-left ch-dark")
        name_document = header.find("h2").contents[0]
        link = header.find("a")
        sub_items = row.find_all("a", class_="list-group-item media")
        list_items = []
        inner_id = 0
        for item in sub_items:
            item_link = item.get("href")
            item_name = item.find("div", class_="lgi-heading").text
            list_items.append({"id": inner_id, "name": item_name, "link": item_link.replace("../", "")})
            inner_id += 1

        hashmap = {"id": id_item, "name": name_document, "link": link.get("href").replace("../", ""), "items": list_items}
        id_item += 1
        items.append(hashmap)

    resp = jsonify(items)
    resp.status_code = 200
    return resp


@app.route('/getDocument', methods=['POST'])
def get_document():
    request_link = request.form['link']
    url_level = main + request_link
    page_level = urllib2.urlopen(url_level)
    soup_level = BeautifulSoup(page_level, 'html.parser')
    soup_level.prettify()
    table = soup_level.find_all("div", {'class': re.compile(r'card br-1 bgb.*')})
    items = []
    id_item = 0
    for row in table:
        header = row.find("div", {'class': re.compile(r'card-header.*')})
        button = header.find("ul", class_="dropdown-menu")
        if button is not None:
            li = button.find("li")
            list_link_document = li.find_all("a")
            list_link_docs = []
            for link_document in list_link_document:
                list_link_docs.append(link_document.get("href").replace("../", ""))
            name_tag = header.find("div", class_="media-body m-t-5")
            name_document = name_tag.find("h2").contents[0]
            date_document = name_tag.find("small").text
            body = row.find("div", class_="card-body br-t-1 bgb-blue")
            img = body.find("img")
            picture_document = img.get("src").replace("../", "")

            hashmap = {"id": id_item, "name": name_document.strip(), "date": date_document.strip(),
                       "picture": picture_document, "link": list_link_docs}
            id_item += 1
            items.append(hashmap)

    resp = jsonify(items)
    resp.status_code = 200
    return resp


if __name__ == '__main__':
    app.run()